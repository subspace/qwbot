/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef SSHCLIENT_H
#define SSHCLIENT_H

#include <QObject>
#include <QStringList>

class QProcess;
class QDateTime;
class App;
class QHostAddress;

/**
  SSH Client exclusively for connecting to QWNET's central.
  It depends on openssh installed on the system and opens a process
  with the ssh binary to make all the communication.

  @author   Mihawk <luiz@netdome.biz>
  @file     SshClient.h
*/
class SshClient : public QObject
{
  Q_OBJECT

public:
  // Error codes
  enum Error { NoError, ConnectionTimedOutError };

  /**
    Constructor

    @param app    The main application
    @param parent The parent object if possible
  */
  explicit SshClient(App* app, QObject *parent = 0);

  /**
    Destructor
  */
  ~SshClient();

  /**
    Connects to a given ssh server.

    @param user Username
    @param host Hostname
  */
  bool connectToHost(const QString& user, const QString &host, quint16 port);

  /**
    Returns whether we are connected right now.

    @return True if connected, false otherwise
  */
  bool isConnected() const;

  /**
    Disconnects from current server.
  */
  void disconnectFromHost();

  /**
    Writes data to the server.

    @param data Data to write
  */
  void write(const QString& data);

signals:
  /**
    Emitted when an error occurs.

    @param errorCode The error code
  */
  void error(Error errorCode);

  /**
    Emitted when we just connected to a server.
  */
  void connected();

protected:
  /**
    Handles events that occur periodically like PING PONG and connection timeout.

    @param e Pointer to the event that happened
  */
  void timerEvent(QTimerEvent *e);

private slots:
  /**
    Called when there is data to be read coming from the server.
  */
  void read();

  /**
    Called when the ssh binary process has exited.

    @param exitCode The exit code
  */
  void exited(int exitCode);

  /**
    Called when a simple ping request to an QW server has finished

    @param host The server's address
    @param port The server's port
    @param ms   The server's ping
  */
  void serverPong(const QHostAddress& host, quint16 port, int ms);

  /**
    Called when a simple ping request to an QW server has finished.
    This function reply to the REQ_ASSIGNMENTS request from central.
    The reason this is being done here is because central requests the ping
    in the answer.

    @param host The server's address
    @param port The server's port
    @param ms   The server's ping
  */
  void assignmentsReply(const QHostAddress& host, quint16 port, int ms);

private:
  App*        myApp;               // The app
  QProcess*   myProcess;           // The ssh process
  QRegExp*    myCommandRegex;      // Regex used on every command, so it was made persistent
  bool        myConnectedFlag;     // Are we connected?
  int         myConnectionTimerID; // Times out connection process
  int         myPingTimerID;       // PING? PONG! Checks if the connection is still alive
  int         myPongTimerID;       // PING? PONG! Checks if the connection is still alive
  QStringList myClients;           // List of other clients connected to central
  QString     myUsername;          // My username on central
  QString     myHostname;          // Central's hostname
  quint16     myPort;              // Central's port number
  QStringList myRoles;             // My roles on central
  QStringList myCommands;          // The list of commands that I can issue

  /**
    Parses all commands coming from the server and execute the appropriate
    command.

    @param time        Time the message arrived
    @param command     The command that just arrived
    @param commandData The command data
  */
  void parse(const QDateTime& time, const QString& command, const QString& commandData);

  /**
    Ping and pong functions used to keep the connection alive.
  */
  void ping();
  void pong();

  /**
    Reconnects to the server.
  */
  void reconnect();
};

#endif // SSHCLIENT_H
