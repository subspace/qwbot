#!/bin/bash

BOTPATH="$(pwd)"
LIBPATH="$BOTPATH/libqwclient/"
if [ "$(which realpath)" != "" ]; then
	LIBPATH="$(realpath $LIBPATH)"
fi

echo BOTPATH: "$BOTPATH"
echo LIBPATH: "$LIBPATH"

while [ 1 ]; do
	echo "started at $(date)" | tee -a runlog.log
	LD_LIBRARY_PATH="$LIBPATH" stdbuf -oL ./cimsqwbot | tee -a runlog.log
	echo "crashed at $(date) - lol, we never crash." | tee -a runlog.log

	sleep 3
done
