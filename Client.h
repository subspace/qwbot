/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef CLIENT_H
#define CLIENT_H

#include "QWClient.h"
#include <QString>
#include <QStringList>
#include <QList>
#include <QTime>
#include "ServerQuery.h"

class App;
class QTime;
class QTimer;
class ActiveClient;

/**
  The QW BOT itself.

  @author   Mihawk <luiz@netdome.biz>
  @file     Client.h
*/
class Client : public QWClient
{
public:
  /**
    Constructor.

    @param app Pointer to the application
    @param ac  Pointer to the monitoring class
    @param supportsSendPrivate Indicates if this server supports s-p command (All messages are sent privately to the user on the server)
  */
  Client(App* app, ActiveClient* ac);

  /**
    Destructor.
  */
  ~Client();

  /**
    Connects the bot to the QW server specified.

    @param host Server host
    @param port Server port
  */
  void connect(const char *host, quint16 port);

  /**
    Runs the QW client, must be called everyframe or so
    otherwise the bot won't run.
  */
  void run();

  /**
    Disconnects the QW client from the server.
  */
  void disconnect();

  /**
    Says something on the server we are in.
    If the nickname is specified s-p will be used if possible.
  */
  void say(const QString& msg, const QString& nickName = "");

  /**
    Sets the bot team.
  */
  void setTeam(const QString& msg);

  /**
    Returns whether we are fully connected to a server.

    @return True if fully connected, otherwise false
  */
  bool isOnServer() const;

  /**
    Returns whether the frequencies are muted.

    @return True if the frequency is muted, otherwise returns false
  */
  bool isQWMuted() const;
  bool isSpamMuted() const;

  /**
    Sets the player list that are on this server
    including the bot in it. This information comes
    from the Query object outside this class.
    FIXME: Make the bot parse the player list inside itself.

    @param playerList The player list
  */
  void setPlayerList(PlayerList &playerList);

  /**
    Sets the maxclients that can connect to this server
    currenctly this information comes from the Query object.
    FIXME: Make the bot gather this info himself, since he is
    connected to the server.

    @param  maxClients  The maximum number of clients that can connect to this
                        particular server
  */
  void setMaxClients(int maxClients);

  /**
    Sets the client to try to autodetect s-p support.
  */
  void setAutoDetectSP(bool autoDetect);

protected:
  /**
    Called everytime the level changes.

    @param playerNum
    @param levelName
    @param gravity
    @param stopSpeed
    @param maxSpeed
    @param spectatorMaxSpeed
    @param accelerate
    @param airAccelerate
    @param waterAccelerate
    @param friction
    @param waterFriction
    @param entGravity
  */
  void onLevelChanged(int playerNum, const char *levelName, float gravity, float stopSpeed, float maxSpeed, float spectatorMaxSpeed, float accelerate, float airAccelerate, float waterAccelerate, float friction, float waterFriction, float entGravity);

  /**
    Called when the client is disconnected from the server.
  */
  void onDisconnect();

  /**
    Called when the client receives information to be printed on the screen.

    @param level Message level
    @param msg   The message
  */
  void onPrint(int level, const char *msg);

  /**
    Called on (respectively):
    Challenge response.
    Connection acknowledged.
    Connected. (Not fully connected yet, signon start)
  */
  void onChallenge();
  void onConnection();
  void onConnected();

  /**
    Called when download of a file started.

    @param fileName The file to be downloaded
  */
  void onDownloadStarted(const char *fileName);

  /**
    Called when the last download finished.
  */
  void onDownloadFinished();

  /**
    Called when the progress for a download changed.

    @param percent Actual download percentage
  */
  void onDownloadProgress(int percent);

  /**
    Called when the server stuffs cmd into the client.

    @param cmd The command being stuffed into the client
  */
  void onStuffedCmd(const char *cmd);

  /**
    Called when an error occurs.

    @param description The error description
  */
  void onError(const char *description);

  /**
    Called when the client receives an out-of-band print.

    @param msg The message to be printed
  */
  void onOOBPrint(const char *msg);

private:
  // The monitor that monitors the server that this client connects to
  ActiveClient*     myActiveClient;

  // Pointer to the application
  App*							myApp;

  // The last line printed into our client
  QString						myPrintLine;

  // Flag to avoid information to repeat on the client
  bool							myDownloadProgressPrintedFlag;

  // Flag to indicate if the client is fully connected
  bool              myOnServerFlag;

  // Flags to indicate whether frequencies are muted or not
  bool              mySpamMutedFlag;
  bool              myQWMutedFlag;

  QTimer*           myKeepNickTimer;    // Interval in which the bot tries to retake his nickname

  // Flood timers
  QTimer*           myFloodTimer;            // Floodtimer for all commands
  QTimer*           myBroadcastFloodTimer;   // Floodtimer for .qw|.spam broadcasts

  // Used to calculate how much time is there left on the flood timers
  QTime             myFloodTimerStart;
  QTime             myBroadcastFloodTimerStart;

  // Used to avoid information printed countless times
  bool							myFloodMsgPrinted;

  // Indicates whether this server supports s-p command
  QTimer*           mySPDetectionTimer; // Timer to wait for a say s-p command reply (if it replies there is support for it)
  bool              mySPSupport;        // Do we have SP support?
  bool              mySPAutoDetect;     // If this is true the client will try to auto-detect the SP support

  // List of players on this server we are connected to
  PlayerList        myPlayerList;

  // Max clients allowed on this server
  int               myMaxClients;

  QTimer*           myCmdScheduledTimer; // Timer for one scheduled command
  QString           myCmdScheduled; // The scheduled command

  /**
    Schedules a command for execution after the time specified expires

    @param cmd The command
    @param time The time to wait for the command to be sent
  */
  void scheduleCmd(const QString& cmd, int time);

  /**
    Prints a message directly to the console or telnet user.

    @param msg The message

  */
  void print(const QString& msg);

  /**
    Parses the last line that was printed.
  */
  void parsePrintedLine();

  /**
    Returns the player count on this server.

    @return The player count
  */
  int  playerCount() const;

  /**
    Returns the maximum number of clients allowed on this server.

    @return The maximum number of clients allowed
  */
  int  maxClients() const;

  /**
    Detects whether this server supports s-p command or not.
  */
  void spDetection();

  /**
    Parses any namefun string and converts it to readable text.

    @param string The text to be parsed
  */
  static QString parseNameFun(const QString& string);
};

#endif // CLIENT_H
