/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef APP_H
#define APP_H

#include <QCoreApplication>
#include <QList>
#include <QSettings>
#include <QHostAddress>
#include <QStringList>

class SshClient;
class QTcpSocket;
class QTcpServer;
class Client;
class ActiveClient;

/**
  The application core.

  @author  Mihawk <luiz@netdome.biz>
  @file    App.h
*/
class App : public QCoreApplication
{
  Q_OBJECT
public:
  /**
    App contructor.

    @param  argc  Command line param count
    @param  argv  Command line param string array
  */
  explicit              App(int &argc, char **argv);

  /**
    App Destructor.
  */
  ~App();

  const QList<ActiveClient*> clients() const;

  /**
    Prints directly to console and to anyone connected via telnet.

    @param  msg The message
  */
  void                  print(const QString& msg);

  /**
    Those two functions broadcast messages to all QW servers that
    the application is connected to.
    The first one return the server and player count that the message
    has reached.
    The second just broadcasts the message to all QW servers ignoring the
    ignoredClient param.
  */
  void                  broadcast(const QString& msg, int *serverCount, int *userCount);
  void                  broadcast(const QString& msg, ActiveClient* ignoredClient = 0);

  /**
    Request central to broadcast a message.

    @param  type    The type of message QDEV QWalt
    @param  user    The user that is broadcasting
    @param  server  The server where the message is coming from
    @param  message The message
  */
  void                  requestBroadcast(const QString& type, const QString& user, const QString& server, const QString& message);

  /**
    Sets the reply hash comming from central and starts timer
    Until the timer is running the application will increment the user count for the
    hash specified.

    @param  serverAddress The address of the server that requested the broadcast
    @param  hash          The hash returned from central server for this message
  */
  void                  setReplyHashAndWaitForReply(const QString& serverAddress, const QString& hash);

  /**
    Increment the reply counters for the server that has the hash specified.

    @param  hash          The broadcasted message hash
    @param  userCount     The irc user count
    @param  channelCount  The irc channel count
    @param  playerCount   The QW player count
    @param  serverCount   The QW server count
  */
  void                  incrementReplyCounters(const QString& hash, int userCount, int channelCount, int playerCount, int serverCount);

  /**
    Gets the servers we are monitoring player counts.

    @param  serverCount   Pointer to variable that will be incremented with the server count
    @param  playerCount   Pointer to variable that will be incremented with the player count
    @param  ignoreClient  Client that counters will be ignored
  */
  void                  activeClientsReplyCounters(int *serverCount, int *playerCount, ActiveClient* ignoreClient = 0);

  /**
    Last 5 messages broadcasted on central.

    @return The list with the messages
  */
  const QStringList&    lastMessages() const;

  /**
    Sets monitored server HostName, this function should be called from SshClient (central)
    in reply to REQ_DNS request (DNS_RE).

    @param serverAddress The server address full string ip:port
    @param hostName      The server hostname (obtained from REQ_DNS request to central)
  */
  void                  setServerHostName(const QString& serverAddress, const QString& hostName);

  /**
    Gets monitored server HostName, this function should be called from QWClient (BOT)
    when broadcasting inside it's own network of bots.

    @param serverAddress The server address full string ip:port
    @return hostName     The server hostname
  */
  QString               serverHostName(const QString& serverAddress) const;

  /**
    Checks if the password specified by the user is correct.

    @param  password  The password
    @return  True if its correct, false otherwise
  */

  bool                  addClient(const QString& hostName, quint16 port, const QString &password = "");

  /**
    Removes a client.

    @param  hostName  hostname
    @param port port
    @return  True if its removed, false otherwise
  */
  bool                  removeClient(const QString& hostName, quint16 port);
protected:
  /**
    App's MainLoop is here.
  */
  void									timerEvent(QTimerEvent *e);

private:
  // Socket for the TelNet administration
  // Server for handling connections to the TelNet administration
  QTcpSocket*           mySocket;
  QTcpServer*           myServer;
  bool                  mySocketConnectedFlag; // Indicates whether there is someone connected to the TelNet administration

  // SSH Client connected to central
  SshClient*            mySshClient;

  // List of Servers we are monitoring
  QList<ActiveClient*>  myClients;

  // Main loop timer identifier
  int                   myClientsFrameTimerID;

  // Last 5 messages list
  QStringList           myLastMessages;

  // Indicates whether I've requested the hostnames, used to avoid requesting the entire scheduled hour
  bool                  myHostNamesRequested;

  /**
    Loads the server list from the config file.
  */
  void                  loadServerList();

  /**
    Saves the current server list to the config file.
  */
  void                  saveServerList();

  /**
    Disconnect/Stop Monitoring from all servers.
  */
  void                  cleanup();

  /**
    Adds message to the history of 5 messages.

    @param  msg The message
  */
  void                  addMessageToHistory(const QString& msg);

  /**
    Parses the command line parameters.

    @return False if the parameters failed to be parsed, true otherwise
  */
  bool                  parseCommandLine();

  /**
    Requests the hostnames of all servers being monitored.
    Those hostnames are obtained from central, that has a smart
    hostname lookup system.
  */
  void                  requestCachedHostNames();

private slots:
  /**
    Called everytime we are fully connected to central.
  */
  void                  onCentralConnection();

};

#endif // APP_H
