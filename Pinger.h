/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef PINGER_H
#define PINGER_H

#include <QObject>
#include <QTime>
#include <QHostAddress>

class QUdpSocket;

/**
  This class is used to ping any QuakeWorld server

  @author   Mihawk <luiz@netdome.biz>
  @file     Pinger.h
  @date     27/10/2012
*/
class Pinger : public QObject
{
  Q_OBJECT
public:
  /**
    Constructor.

    @param host The host we are going to ping
    @param port The host's port
  */
  Pinger(const QHostAddress& host, quint16 port, QObject *parent = 0);

  /**
    Starts the pinging process
  */
  void ping();

signals:
  /**
    Called when we received an answer from the server
    sending the time diff between the call to ping() and now
    as the ping.

    @param host The host we are pinging
    @param port The host's port
    @param ping The ping we got from this server
  */
  void finished(const QHostAddress& host, quint16 port, int ping);

protected:
  /**
    Called when the pinging process times out
    this timeout is set to 1000ms.

    @param e The timer event
  */
  void timerEvent(QTimerEvent *e);

private slots:
  /**
    Called when we received an answer from the server
    sending the time diff between the call to ping() and now
    as the ping. This function will emit the finished signal.
  */
  void pong();

private:
  QUdpSocket*  mySocket; // Our socket
  QTime        myTime;   // Time used for calculating the ping
  QHostAddress myHost;   // Host to be pinged
  quint16      myPort;   // Host's port
  int          myTimerID;// Timer used for timing out an ping request
};

#endif // PINGER_H
