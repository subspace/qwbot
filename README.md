# _QuakeWorld Bot_
You want to install this QuakeWorld Bot, or want to get more information about it? 
You've come to the right place. So to say, one of the right places. ;) You can also find the devs on irc://irc.quakenet.org/qwnet.

## How to install it
We got an installation script to do the work of compiling the source files and putting the binaries to a target directory. It will even run the bot for the first time. Why? Because, on the first run, it will create a configuration file.
You only need to have git, gcc(g++), Qt(libs: qt-core, libqt4-dev(debian)) and the usual make installed on your system. They're pre-requisites. The script checks for them too. If they don't exist on your system, you can either install them, or you could ask the devs for a statically linked binary package.

So, here's the thing:

>$ git clone https://gitlab.netdome.biz/community-messaging-project/qwbot.git
>$ cd qwbot
>$ ./install.sh \<target folder\>

## How do I get the Bot to connect to a server?
There are two ways to do that; the automatic way, when connected to the "central" and the manual way, by configuring the config file.

### Add a server via config file:
In the config file (currently named qwbot.cfg) there is the [Servers] section.
First, set the "size" - the count of the servers that are to be monitored.
Then, set the server addresses themselves.

_Example:_
>[Servers]
>size=2
>1\address=123.123.123.123:27101
>2\address=hostname:27500

We know, this is not really intuitive, but the whole thing is designed to normally work automatically, controlled by a "central" server - and that's the other method.