/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QList>
#include <QString>

class QSettings;

/**
  This class provides direct settings file access for the whole application.

  @author   Mihawk <luiz@netdome.biz>
  @file     Settings.h
*/
class Settings
{
public:
  // Server information struct
  // Struct to save server information from the config file
  struct Server
  {
    QString address;
    quint16 port;
    QString password; // Used for spectator VIP slots
  };
  typedef QList<Server> ServerList;

  /**
    The class single instance.

    @return The settings class
  */
  static Settings*  globalInstance();

  /**
    Changes the config file used by us.

    @param fileName The filename to be used
    @return True if the file was loaded successfully, false otherwise
  */
  bool              changeConfigFile(const QString& fileName);

  /**
    Gets and sets the serverlist
  */
  ServerList        serverList();
  void              setServerList(ServerList& list);

  /**
    Config file direct parameter accessors
  */
  QString           quakeFolder() const; // QuakeWorld folder
  QString           botName() const; // Bot name
  int               botPing() const; // Bot ping
  int               botTopColor() const; // Bot shirts color
  int               botBottomColor() const; // Bot pants color
  bool              botSpectator() const; // Bot joins as spectator
  int               floodProtTime() const; // Flood protection time
  int               qwFloodProtTime() const; // Flood protection time for .qw command
  int               spamFloodProtTime() const; // Flood protection time for .spam command
  unsigned int      queryInterval() const; // Interval between server status queries (check if there are or there arent players on a given server)
  int               timeToSayHiAfterConnected() const; // OBSOLETE
  int               timeToWaitForCountReply() const; // Time to wait from a reply from central about the amount of people that the message we broadcasted has reached
  bool              developerMode() const; // Developer mode
  QString           sshUserName() const; // Username on central
  QString           sshHostName() const; // Central's HostName
  quint16           sshPort() const; // Central's port
  int               refreshHostNamesHour() const; // An specific hour that the bot must refresh his hostname list
  int               maxServers() const; // Maximum servers that we can monitor

  /**
    Save current settings to the file.
  */
  void              save();

private:
  static Settings*  ourInstance; // Myself
  static QSettings* ourSettings; // The QSettings object

  // Disable direct object creation, copy and destruction
  Settings();
  ~Settings();
  Settings(Settings&) {}
};

#endif // SETTINGS_H
