#!/usr/bin/env bash
# vim: expandtab tabstop=2 shiftwidth=2 softtabstop=2 autoindent:
# kate: space-indent on; indent-width 2; mixedindent off;

NEEDS="git qmake make g++"


DIR_LIBQWCL="libqwclient"
SRCPATH_QWBOT=$(pwd)
# we certainly hope we are in our repository. 
cd ${SRCPATH_QWBOT}/${DIR_LIBQWCL}/
SRCPATH_LIBQWCL=$(pwd)
cd ${SRCPATH_QWBOT}


BIN_QWBOT="cimsqwbot"
CFG_QWBOT="cimsqwbot.cfg"
STARTER="start_cmp_qwbot.sh"



INST_LOCATION=""



function usage() {
  echo "Installation Script for the QWBOT of the community messaging project."
  echo "Usage: ${0} <target-directory>"
  exit
}

for p in $NEEDS; do
  TEST=$(which $p 2> /dev/null)

  if [ ! -f "$TEST" ]; then
    echo "Sorry, you don't seem to have '$p' installed."
    echo "You need these programs before this script will run: $NEEDS"
    exit 
  fi
done


# on first run, we need a target location for binaries, lets explain usage and check for the argument OR the location file.....


# if we have not found the location file, get location from argument.. and go on
if [ "${INST_LOCATION}" == "" ]; then
  if [ "$1" != "" ]; then
    INST_LOCATION="${1}"
    echo "Using target-directory ${INST_LOCATION}"
    echo
    mkdir -p "${INST_LOCATION}"
    if [ -d "${INST_LOCATION}" ]; then
      # worked, we save the binary target location in a small file
      echo "INST_LOCATION=\"${INST_LOCATION}\"" > install.location
    fi
  else
    # if we found the location file, get location, go on..
    if [ -f "install.location" ]; then
      source ./install.location
      echo "Read target-directory from file ./install.location - it is: ${INST_LOCATION}"
      echo "Delete the file if you want to specify a new target directory."
      echo
    else
      # if no argument given AND there is no location file, then give usage info.    
      usage
    fi
  fi
fi


# now, we have a target location... lets go on and pull
# if we are in our git directory then update all
git pull

cd "${SRCPATH_QWBOT}"
git submodule update --init --recursive


# we always make them both...
cd "$SRCPATH_LIBQWCL"
qmake
make clean
make

cd "${SRCPATH_QWBOT}"
qmake
make clean
make



if [ -d "${INST_LOCATION}" ]; then
  #copy assets over
  echo "Copying qwbot binary and starting script..."
  cp "${BIN_QWBOT}" "${INST_LOCATION}"
  cp -v "${STARTER}" "${INST_LOCATION}"

  echo "Copying qwclient library..."
  mkdir -p "${INST_LOCATION}/libqwclient"
  cp -vd ${SRCPATH_LIBQWCL}/*.so.* "${INST_LOCATION}/libqwclient/"
  
  # change into that target directory
  cd ${INST_LOCATION}
  
  # first start there:
  if [ -f "./${BIN_QWBOT}" ]; then
    LD_LIBRARY_PATH=${SRCPATH_LIBQWCL} ./${BIN_QWBOT} &
    PID=$!
    echo PID $PID
    sleep 1
    kill -9 $PID > /dev/null
  else
    echo
    echo "It seems there have been problems compiling this thing! The binary ./${BIN_QWBOT} is missing."
    echo "This is awkward. Probably some dependency was missing or had a wrong version while compilation."
    echo "Better go have a chat to the devs at irc://quakenet.org/qwnet"
    echo
    exit
  fi
  # cfg file should have been created by the first start of the qwbot.

  # now, lets check the starting script. It should be executable.
  if [ -f "${STARTER}" ]; then
    chmod u+x "${STARTER}"
  fi


  cd "${INST_LOCATION}"
  echo
  echo "The installation is now ready to run. BUT..."
  echo 
  echo "1. Please know, that you need maps. The bot will start downloading them often, which takes time."
  echo "   It is advised, you get some maps already and put them in the appropriate gamedir under"
  echo "   ${INST_LOCATION}"
  echo "   gamedir qw example: ${INST_LOCATION}/qw/maps"
  echo "   gamedir fortress example: ${INST_LOCATION}/fortress/maps"
  echo
  echo "2. Please edit the ${CFG_QWBOT}"
  echo "   For an overview of configuration options, please see the wiki at:"
  echo "   https://gitlab.netdome.biz/community-messaging-project/qwbot/wikis/home"
  echo
  echo "3. Now, it's cool to run:"
  echo "   cd "${INST_LOCATION}" && ./${STARTER}"
  echo
  echo "4. If you want to update your bot at a later time, go to ${SRCPATH_QWBOT} and run me (${0}) again."
  echo
  echo "Have fun."
  echo
else
  #missing
  echo "For some reason I can't create or access the target directory ${INST_LOCATION} ...is it alive?"
  exit
fi



