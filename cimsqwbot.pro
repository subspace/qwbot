TEMPLATE = app
CONFIG += console
QT     += network
QT     -= gui

SOURCES += main.cpp \
    Client.cpp \
    App.cpp \
    ServerQuery.cpp \
    SshClient.cpp \
    ActiveClient.cpp \
    Settings.cpp \
    Pinger.cpp

HEADERS += \
    Client.h \
    App.h \
    ServerQuery.h \
    SshClient.h \
    ActiveClient.h \
    Settings.h \
    Pinger.h



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/libqwclient/release/ -lqwclient
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/libqwclient/debug/ -lqwclient
else:unix:!symbian: LIBS += -L$$PWD/libqwclient/ -lqwclient

INCLUDEPATH += $$PWD/libqwclient
DEPENDPATH += $$PWD/libqwclient
