/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include <QTimer>
#include <QTime>

#include "ServerQuery.h"
#include "ActiveClient.h"
#include "Client.h"
#include "App.h"
#include "Settings.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ActiveClient::ActiveClient(App *app, const QString& password, QObject *parent):
  QObject(parent),
  myApp(app),
  myClient(new Client(app, this)),
  myQuery(new ServerQuery(this)),
  myDisconnectTime(new QTime()),
  myBroadcastReplyTimer(new QTimer(this)),
  myQueryTimer(new QTimer(this)),
  myQueryInterval(Settings::globalInstance()->queryInterval()),
  myHasHostNameFlag(false),
  myUniqueUserCount(0),
  myUniqueChannelCount(0),
  myUniqueServerCount(0),
  myUniquePlayerCount(0),
  myReplyTimerWasActive(false)
{
  connect(myQuery, SIGNAL(finished()), SLOT(queryFinished()));
  connect(myQuery, SIGNAL(error(ServerQuery::Error)), SLOT(queryError(ServerQuery::Error)));

  myQueryTimer->setSingleShot(true);

  myClient->setPassword(password.toLatin1().data());
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ActiveClient::~ActiveClient()
{
  delete myClient;
  delete myDisconnectTime;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Client* ActiveClient::client()
{
  return myClient;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
quint8 ActiveClient::playerCount() const
{
  return myQuery->playerList().size();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::setAddress(const QString &hostName, const QHostAddress &hostAddress, quint16 port)
{
  myQuery->setAddress(hostAddress, port);
  setHostName(hostName);
  return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool ActiveClient::hasHostName() const {
  return myHasHostNameFlag;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QHostAddress& ActiveClient::address() const {
  return myQuery->address();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
quint16 ActiveClient::port() const {
  return myQuery->port();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::queryError(ServerQuery::Error)
{
//  myQueryTimer->start(myQueryInterval);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::queryFinished()
{
  PlayerList playerList = myQuery->playerList();
  int playerCount = playerList.size();
  myClient->setPlayerList(playerList);
  myClient->setMaxClients(myQuery->serverRuleValue("maxclients").toInt());

//  myQueryTimer->start(myQueryInterval);

  /* If the client is disconnect check if there is at least one player connected to the server */
  if(myClient->state() == Client::DisconnectedState)
  {
    if(playerCount > 0 && myDisconnectTime->elapsed() > 10000)
    {
      myApp->print("Players online on server " + serverAddressString() + ". Joining...\n");
      myClient->connect(myQuery->address().toString().toLatin1(), myQuery->port());
    }
    return;
  }

  /* If the client is connected and left alone on the server, disconnect yourself */
  if(myClient->state() == Client::ConnectedState)
  {
    if(playerCount == 1 && myClient->isOnServer())
    {
      myApp->print("I was left alone on " + serverAddressString() + ". Leaving...\n");
      myClient->disconnect();
      myDisconnectTime->restart();
      return;
    }
  }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::run()
{
  /* Say the broadcast count */
  if(!myBroadcastReplyTimer->isActive() && myReplyTimerWasActive)
  {
    myApp->activeClientsReplyCounters(&myUniqueServerCount, &myUniquePlayerCount, this); //add our servers to the list
    myClient->say("Sent to " + QString::number(myUniqueChannelCount) + " channels " + QString::number(myUniqueUserCount) + " users and to " + QString::number(myUniqueServerCount) + " servers " + QString::number(myUniquePlayerCount) + " players.");
    myUniqueServerCount = myUniquePlayerCount = myUniqueChannelCount = myUniqueUserCount = 0;
  }
  myReplyTimerWasActive = myBroadcastReplyTimer->isActive();

  /* Query the serverinfo for player count if not already querying */
  if(!myQueryTimer->isActive())
  {
    myQuery->query();
    myQueryTimer->start(myQueryInterval);
  }

  /* Run the client */
  if(myClient->state() != Client::DisconnectedState)
    myClient->run();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QString ActiveClient::serverAddressString()
{
  return QString(myQuery->address().toString() + ':' + QString::number(myQuery->port()));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QString ActiveClient::serverHostNameString()
{
  return QString(myHostName + ':' + QString::number(myQuery->port()));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::setReplyHashAndWaitForReply(const QString &hash)
{
  myReplyHash = hash;

  /* Wait for reply */
  myBroadcastReplyTimer->setSingleShot(true);
  myBroadcastReplyTimer->start(Settings::globalInstance()->timeToWaitForCountReply()*1000);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QString& ActiveClient::replyHash() const
{
  return myReplyHash;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool ActiveClient::isWaitingReply() const
{
  return myBroadcastReplyTimer->isActive();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::incrementReplyCounters(int userCount, int channelCount, int playerCount, int serverCount)
{
  if(!myBroadcastReplyTimer->isActive())
    return;

  myUniqueUserCount += userCount;
  myUniqueChannelCount += channelCount;
  myUniqueServerCount += serverCount;
  myUniquePlayerCount += playerCount;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ActiveClient::setHostName(const QString &hostName)
{
  myHostName = hostName;
  if (hostName == myQuery->address().toString()) // If hostname == ip address then we have nothing and we can query central for the DNS
    myHasHostNameFlag = false;
  else
    myHasHostNameFlag = true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QString& ActiveClient::hostName() const
{
  return myHostName;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int ActiveClient::ping() const
{
  return myQuery->ping();
}
