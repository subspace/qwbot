/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "Settings.h"
#include <QSettings>
#include <QCoreApplication>
#include <QStringList>
#include <stdio.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Settings* Settings::ourInstance = NULL;
QSettings* Settings::ourSettings;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Settings::Settings()
{
  ourSettings = new QSettings(QCoreApplication::applicationDirPath() + "/cimsqwbot.cfg", QSettings::IniFormat);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Settings::~Settings()
{
  delete ourSettings;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool Settings::changeConfigFile(const QString &fileName)
{
  delete ourSettings;
  ourSettings = new QSettings(fileName, QSettings::IniFormat);

  QSettings::Status status = ourSettings->status();
  if(status != QSettings::NoError)
  {
    printf("Failed to load config file [%s]. Falling back to default config file.\n", fileName.toLatin1().data());
    delete ourSettings;
    ourSettings = NULL;
    return false;
  }
  return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Settings* Settings::globalInstance()
{
  if(!ourInstance)
    ourInstance = new Settings();
  return ourInstance;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Settings::ServerList Settings::serverList()
{
  ServerList list;

  int size = ourSettings->beginReadArray("servers");
  for(int i = 0; i < size; ++i)
  {
    ourSettings->setArrayIndex(i);
    QStringList svaddr = ourSettings->value("address").toString().split(":");
    Server sv;
    if(svaddr.size() >= 2)
    {
      sv.address = svaddr.at(0);
      sv.port = svaddr.at(1).toUShort();
      if(svaddr.size() > 2)
        sv.password = svaddr.at(2);
    }
    else
      continue;
    list.append(sv);
  }
  ourSettings->endArray();

  return list;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Settings::setServerList(ServerList &list)
{
  ourSettings->beginWriteArray("servers");
  for(int i = 0; i < list.size(); ++i)
  {
    ourSettings->setArrayIndex(i);
    ourSettings->setValue("address", list.at(i).address + ":" + QString::number(list.at(i).port) + ":" + list.at(i).password);
  }
  ourSettings->endArray();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QString Settings::quakeFolder() const
{
  return ourSettings->value("quakeFolder", QCoreApplication::applicationDirPath()).toString();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QString Settings::botName() const
{
  return ourSettings->value("botName", "[ServeMe]").toString();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::botPing() const
{
  return ourSettings->value("botPing", 666).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::botTopColor() const
{
  return ourSettings->value("botTopColor", 11).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::botBottomColor() const
{
  return ourSettings->value("botBottomColor", 12).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool Settings::botSpectator() const
{
  return ourSettings->value("botSpectator", true).toBool();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::floodProtTime() const
{
  return qBound<int>(6, ourSettings->value("floodProtTime", 6).toInt(), 9999);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::qwFloodProtTime() const
{
  return ourSettings->value("qwFloodProtTime", 600).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::spamFloodProtTime() const
{
  return ourSettings->value("spamFloodProtTime", 300).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
unsigned int Settings::queryInterval() const
{
  return ourSettings->value("queryInterval", 1000).toUInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::timeToSayHiAfterConnected() const
{
  return ourSettings->value("timeToSayHiAfterConnected", 7).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::timeToWaitForCountReply() const
{
  return ourSettings->value("timeToWaitForCountReply", 7).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool Settings::developerMode() const
{
  return ourSettings->value("developerMode", false).toBool();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QString Settings::sshHostName() const
{
  return ourSettings->value("sshHostName", "b4r.org").toString();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QString Settings::sshUserName() const
{
  return ourSettings->value("sshUserName", "stomp").toString();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
quint16 Settings::sshPort() const
{
  return ourSettings->value("sshPort", 22).toUInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::refreshHostNamesHour() const
{
  return ourSettings->value("refreshHostNamesHour", 21).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Settings::maxServers() const
{
  return ourSettings->value("maxServers", 100).toInt();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Settings::save()
{
  ourSettings->setValue("quakeFolder", quakeFolder());
  ourSettings->setValue("botName", botName());
  ourSettings->setValue("botPing", botPing());
  ourSettings->setValue("botTopColor", botTopColor());
  ourSettings->setValue("botBottomColor", botBottomColor());
  ourSettings->setValue("botSpectator", botSpectator());
  ourSettings->setValue("floodProtTime", floodProtTime());
  ourSettings->setValue("queryInterval", queryInterval());
  ourSettings->setValue("qwFloodProtTime", qwFloodProtTime());
  ourSettings->setValue("spamFloodProtTime", spamFloodProtTime());
  ourSettings->setValue("timeToSayHiAfterConnected", timeToSayHiAfterConnected());
  ourSettings->setValue("timeToWaitForCountReply", timeToWaitForCountReply());
  ourSettings->setValue("developerMode", developerMode());
  ourSettings->setValue("sshHostName", sshHostName());
  ourSettings->setValue("sshUserName", sshUserName());
  ourSettings->setValue("sshPort", sshPort());
  ourSettings->setValue("refreshHostNamesHour", refreshHostNamesHour());
  ourSettings->setValue("maxServers", maxServers());

  ServerList list = serverList();
  setServerList(list);
}
