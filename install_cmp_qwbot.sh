#!/usr/bin/env bash
# vim: expandtab tabstop=2 shiftwidth=2 softtabstop=2 autoindent:


NEEDS="git qmake make g++"

DIR_LIBQWCL="libqwclient"
DIR_QWBOT="qwbot"

BIN_QWBOT="cimsqwbot"
CFG_QWBOT="cimsqwbot.cfg"

STARTER="startcimsqwbot.sh"

for p in $NEEDS; do
	TEST=$(which $p 2> /dev/null)

	if [ ! -f "$TEST" ]; then
		echo "Sorry, you don't seem to have '$p' installed."
		echo "You need these programs before this script will run: $NEEDS"
		exit 
	fi
done

if [ ! -d "${DIR_QWBOT}" ]; then
  	git clone https://gitlab.netdome.biz/community-messaging-project/qwbot.git ${DIR_QWBOT}
	INSTALL=1
fi

cd ${DIR_QWBOT}
PATH_QWBOT=$(pwd)

cd ${PATH_QWBOT}/${DIR_LIBQWCL}/
LIBPATH=$(pwd)

CONFIGNAME="${PATH_QWBOT}/${CFG_QWBOT}"
STARTSCRIPTNAME="${PATH_QWBOT}/${STARTER}"

if [ "$INSTALL" == "1" ]; then
	# is the configuration file missing? then...
	if [ ! -f "$CONFIGNAME" ]; then
		cd "${PATH_QWBOT}"
		git submodule update --init --recursive

		cd "$LIBPATH"
		qmake
		make

		ln -s ${LIBPATH}/QWClient.h ${PATH_QWBOT}/QWClient.h
		ln -s ${LIBPATH}/qwclient_global.h ${PATH_QWBOT}/qwclient_global.h
		cd "${PATH_QWBOT}"
		qmake
		make

		if [ -f "./${BIN_QWBOT}" ]; then
			LD_LIBRARY_PATH=$LIBPATH ./${BIN_QWBOT} &
			PID=$!
			echo PID $PID
			sleep 1
			kill $PID
		else
			echo
			echo "It seems there have been problems compiling this thing! The binary ./${BIN_QWBOT} is missing."
			echo "This is awkward. Probably some dependency was missing or had a wrong version while compilation."
			echo "Better go have a chat to the devs at irc://quakenet.org/qwnet"
			echo
			exit
		fi
	   # cfg file should have been created by the first start of the qwbot.
	fi

	# now, lets check the starting script. It should be executable.
	if [ -f "$STARTSCRIPTNAME" ]; then
		chmod u+x "$STARTSCRIPTNAME"
	fi

	cd "$PATH_QWBOT"
	echo
	echo "The installation is now ready to run. BUT..."
	echo 
	echo "1. Please know, that you need maps. The bot will start downloading them often, which takes time."
	echo "   It is advised, you get some maps already and put them in the appropriate gamedir under"
	echo "   $PATH_QWBOT"
	echo "   gamedir qw example: $PATH_QWBOT/qw/maps"
	echo "   gamedir fortress example: $PATH_QWBOT/fortress/maps"
	echo
	echo "2. Please edit the $CONFIGNAME"
	echo "   For an overview of configuration options, please see the wiki at:"
	echo "   https://gitlab.netdome.biz/community-messaging-project/qwbot/wikis/home"
	echo
	echo "3. Now, it's cool to run:"
	echo "   cd "$PATH_QWBOT" && ./$STARTER"
	echo
	echo "Have fun."
	echo
else
	echo
	echo "It seems the directories '$DIR_QWBOT' and '$DIR_LIBQWCL' already exist."
	echo "So, we did nothing. If you want a fresh install, delete these directories, but"
	echo "be sure to save your $CFG_QWBOT - if you have a good one."
	echo
	echo "Did nothing."
	echo
fi
