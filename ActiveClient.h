/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#ifndef ACTIVECLIENT_H
#define ACTIVECLIENT_H

#include <QObject>
#include <QString>
#include "ServerQuery.h"

class QHostAddress;
class QTimer;
class QTime;
class Client;
class App;

/**
  Takes care of joining the client to the server in the case there are players
  and leaving when there are nobody.
  It constantly queryies the server using myQuery to gather information from the servers.

  @author Mihawk <luiz@netdome.biz>
  @file   ActiveClient.h
*/
class ActiveClient: public QObject
{
  Q_OBJECT

public:
  /**
    Creates a server monitoring object.

    @param  app    The application creator of this object
    @param  password The password needed to connect to this server if any
    @param  parent The parent object (should be the same as the app param)
  */
  ActiveClient(App* app, const QString &password = "", QObject *parent = 0);

  /**
    Destructor
  */
  virtual ~ActiveClient();

  /**
    Returns the QW Client used.

    @return The client used by us
  */
  Client*           client();

  /**
    Makes this class work, should be called every frame after creation of the object.
  */
  void              run();

  /**
    Set the server that we are going to monitor.

    @param hostName Server hostname
    @param hostAddress Server address
    @param port    Server port
  */
  void              setAddress(const QString& hostName, const QHostAddress &hostAddress, quint16 port);

  const QHostAddress& address() const;
  quint16 port() const;

  /**
    Returns the address of the server we are monitoring.

    @return The server address
  */
  const QString     serverAddressString();

  /**
    Returns the full hostname of the server we are monitoring (host:port).

    @return The server address
  */
  const QString     serverHostNameString();

  /**
    Returns the number of players on the server last time the query was ran.

    @return The number of players
  */
  quint8            playerCount() const;

  /**
    Sets the hash and start waiting for reply from central on that hash.

    @param hash The reply hash
  */
  void              setReplyHashAndWaitForReply(const QString& hash);

  /**
    Returns the reply hash of the last reply.

    @return The hash
  */
  const QString&    replyHash() const;

  /**
    Called to increment the internal reply counters
    Those reply counters are counters of how many people received the message
    that we just broadcasted.

    @param  userCount     The irc user count
    @param  channelCount  The irc channel count
    @param  playerCount   The QW player count
    @param  serverCount   The QW servers count
  */
  void              incrementReplyCounters(int userCount, int channelCount, int playerCount, int serverCount);

  /**
    Tells us if we are wating for the count reply from the server.

    @return True if waiting for reply, false otherwise
  */
  bool              isWaitingReply() const;

  /**
    Set hostname

    @param hostName The hostname
  */
  void              setHostName(const QString& hostName);

  /**
   * @brief hasHostName
   * @return
   */
  bool              hasHostName() const;

  /**
    Get hostname

    @return The hostname
  */
  const QString&    hostName() const;

  /**
    Returns the server ping

    @return The ping
  */
  int               ping() const;

private slots:
  /**
    Called when a query gathering server information has just finished successfully.
  */
  void              queryFinished();

  /**
    Called when a query fails on the process of gathering information the server.

    @param  err Error code
  */
  void              queryError(ServerQuery::Error err);

private:
  // The application that has created us
  App*              myApp;

  // The QW client connects to the server
  Client*           myClient;

  // The information gathering query object
  ServerQuery*      myQuery;

  // Timer to avoid too fast reconnections
  QTime*            myDisconnectTime;

  // Count server reply while timer is active
  QString           myReplyHash;
  QTimer*           myBroadcastReplyTimer;

  // How much time to wait b4 next query for player information
  QTimer*           myQueryTimer;
  quint32           myQueryInterval;

  // Server HostName used on reply
  QString           myHostName;
  bool              myHasHostNameFlag;

  // Reply counters
  int               myUniqueUserCount;
  int               myUniqueChannelCount;
  int               myUniqueServerCount;
  int               myUniquePlayerCount;

  bool              myReplyTimerWasActive; // For printing the reply counters
};

#endif // ACTIVECLIENT_H
