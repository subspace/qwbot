/*
GNU General Public License version 3 notice

Copyright (C) 2012 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/

#include "Pinger.h"
#include <QUdpSocket>
#include <QTime>
#include <QTimerEvent>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Pinger::Pinger(const QHostAddress &host, quint16 port, QObject *parent) :
  QObject(parent),
  mySocket(new QUdpSocket(this)),
  myHost(host),
  myPort(port),
  myTimerID(-1)
{
  mySocket->connectToHost(host, port);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Pinger::ping()
{
  mySocket->write("\xff\xff\xff\xffk\n");
  mySocket->waitForBytesWritten(500);
  myTime.start();
  myTimerID = startTimer(1000);
  connect(mySocket, SIGNAL(readyRead()), SLOT(pong()));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Pinger::pong()
{
  emit finished(myHost, myPort, myTime.elapsed());
  disconnect(mySocket, SIGNAL(readyRead()), this, SLOT(pong()));
  killTimer(myTimerID);
  myTimerID = -1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Pinger::timerEvent(QTimerEvent *e)
{
  if(e->timerId() != myTimerID)
    return;

  emit finished(myHost, myPort, myTime.elapsed());
  disconnect(mySocket, SIGNAL(readyRead()), this, SLOT(pong()));
  killTimer(myTimerID);
  myTimerID = -1;
}
